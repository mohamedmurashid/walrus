package com.walrus.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.walrus.app.R;
import com.walrus.app.dp.News;

import java.util.ArrayList;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<News> mNews;

    RequestOptions mRequestOptions;
    ClickListener mClickListener;


    public NewsAdapter(Context context, ArrayList<News> news, ClickListener clickListener) {
        mContext = context;
        mNews = news;
        mClickListener = clickListener;
        mRequestOptions = new RequestOptions();
        mRequestOptions = mRequestOptions.transforms(new CenterCrop(), new RoundedCorners(16));
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_news, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tv_author.setText(mNews.get(position).getAuthor());
        holder.tv_title.setText(mNews.get(position).getTitle());
        holder.tv_content.setText(mNews.get(position).getContent());

        Glide.with(mContext)
                .load(mNews.get(position).getUrlToImage())
                .centerCrop()
                .apply(mRequestOptions)
                .into(holder.iv_news);
    }

    @Override
    public int getItemCount() {
        return Math.min(mNews.size(), 25);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tv_author, tv_title, tv_content, tv_full_news;
        ImageView iv_news;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_author = itemView.findViewById(R.id.tv_author);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_content = itemView.findViewById(R.id.tv_content);
            iv_news = itemView.findViewById(R.id.iv_news);
            tv_full_news = itemView.findViewById(R.id.tv_full_news);

            tv_full_news.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mClickListener.onClickFullNews(getAdapterPosition());
        }
    }

    public interface ClickListener {
        void onClickFullNews(int position);
    }
}
