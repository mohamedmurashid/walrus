package com.walrus.app.dp;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.walrus.app.util.Application;
import com.walrus.app.util.Constant;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {News.class}, version = 1)
@TypeConverters(NewsListConverter.class)
public abstract class AppDatabase extends RoomDatabase {

    public abstract NewsDao newsDao();

    private static volatile AppDatabase INSTANCE;
    public final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(Constant.NUMBER_OF_THREADS_DB_OPERATION);

    public static AppDatabase getDatabase(final Application context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context,
                            AppDatabase.class, Constant.DATABASE_NAME)
                            .allowMainThreadQueries()
                            .build();
                }
            }
        }
        return INSTANCE;
    }

}
