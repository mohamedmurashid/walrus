package com.walrus.app.dp;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.walrus.app.util.Constant;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface NewsDao {

    @Query("SELECT * FROM "+ Constant.NEWS_TABLE_NAME)
    List<News> getNews();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(ArrayList<News> news);

    @Query("DELETE FROM "+ Constant.NEWS_TABLE_NAME)
    public void clearNews();
}
