package com.walrus.app.dp;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

public class NewsListConverter implements Serializable {

    @TypeConverter
    public String fromNewsList(List<News> news) {
        if (news == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<News>>() {
        }.getType();
        return gson.toJson(news, type);
    }

    @TypeConverter
    public List<News> toNewsList(String optionValuesString) {
        if (optionValuesString == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<News>>() {
        }.getType();
        return gson.fromJson(optionValuesString, type);
    }

}