package com.walrus.app.model;

import com.walrus.app.dp.News;

import java.util.ArrayList;

public class NewsResponse {
    private String status;
    private int totalResults;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    public ArrayList<News> getArticles() {
        return articles;
    }

    public void setArticles(ArrayList<News> articles) {
        this.articles = articles;
    }

    private ArrayList<News> articles;


}
