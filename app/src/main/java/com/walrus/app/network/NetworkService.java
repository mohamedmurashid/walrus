package com.walrus.app.network;



import com.walrus.app.model.NewsResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NetworkService {

    @GET("v2/top-headlines")
    Call<NewsResponse> getTrendingNewsList(@Query("country") String country,
                                           @Query("apiKey") String apiKey);
}