package com.walrus.app.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.walrus.app.util.Application;
import com.walrus.app.BuildConfig;
import com.walrus.app.util.Constant;
import com.walrus.app.dp.News;
import com.walrus.app.model.NewsResponse;
import com.walrus.app.model.Resource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsRepository {

    public MutableLiveData<Resource<NewsResponse>> getNews() {
        final MutableLiveData<Resource<NewsResponse>> newsResponse = new MutableLiveData<>();

        List<News> news = Application.getInstance().getAppDatabase().newsDao().getNews();
        NewsResponse newsResponseLocalData = new NewsResponse();
        if(news != null) {
            newsResponseLocalData.setTotalResults(news.size());
            newsResponseLocalData.setStatus("Ok");
            newsResponseLocalData.setArticles(new ArrayList<>(news));
            newsResponse.setValue(Resource.success(newsResponseLocalData));
            Log.d(Constant.TAG, "getNews: Local "+newsResponseLocalData.getArticles());
        }

        Call<NewsResponse> call  = Application.getInstance().getNetworkService()
                .getTrendingNewsList(Constant.COUNTRY_QUERY, BuildConfig.SERVER_API_KEY);
        Log.d(Constant.TAG, "getNews: "+call);
        call.enqueue(new Callback<NewsResponse>() {
            @Override
            public void onResponse(Call<NewsResponse> call, Response<NewsResponse> response) {
                NewsResponse body = response.body();
                if (body != null) {
                    newsResponse.setValue(Resource.success(body));
                    ArrayList<News> news = body.getArticles();
                    Collections.sort(news, new CustomComparator());
                    insert(news);
                } else {
                    newsResponse.setValue(Resource.<NewsResponse>error("No Data", null));
                }
            }
            @Override
            public void onFailure(Call<NewsResponse> call, Throwable t) {
                newsResponse.setValue(Resource.<NewsResponse>error(t.getMessage(),null));

            }
        });
        return newsResponse;
    }

    void insert(ArrayList<News> news) {
        Application.getInstance().getAppDatabase().databaseWriteExecutor.execute(() -> {
            Application.getInstance().getAppDatabase().newsDao().clearNews();
            Application.getInstance().getAppDatabase().newsDao().insert(news);
        });
    }

    public class CustomComparator implements Comparator<News> {
        @Override
        public int compare(News o1, News o2) {
            if(o1.getAuthor() != null && o2.getAuthor() !=null) {
                return o1.getAuthor().compareTo(o2.getAuthor());
            }
            return 0;
        }
    }
}
