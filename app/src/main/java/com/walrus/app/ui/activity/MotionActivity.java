package com.walrus.app.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.motion.widget.MotionLayout;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.walrus.app.R;
import com.walrus.app.util.Constant;

public class MotionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_motion);

        findViewById(R.id.tv_view_news_list).setOnClickListener(v -> {
            startActivity(new Intent(MotionActivity.this, NewsActivity.class));
            finish();
        });

    }
}