package com.walrus.app.ui.activity;

import android.os.Bundle;

import com.walrus.app.R;
import com.walrus.app.ui.fragment.NewsDetailsFragment;
import com.walrus.app.ui.fragment.NewsListFragment;

import androidx.appcompat.app.AppCompatActivity;

public class NewsActivity extends AppCompatActivity implements NewsListFragment.ClickListenerFromNewsList{

    NewsDetailsFragment mNewsDetailsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        initView();
    }

    private void initView() {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, NewsListFragment.newInstance()).commit();
    }

    @Override
    public void onClickFullNews(String title, String url) {
        mNewsDetailsFragment = NewsDetailsFragment.newInstance(title, url);
        getSupportFragmentManager().beginTransaction().add(R.id.container, mNewsDetailsFragment).commit();
    }

    @Override
    public void onBackPressed() {
        if (mNewsDetailsFragment.isResumed()) {
            getSupportFragmentManager().beginTransaction().remove(mNewsDetailsFragment).commit();
            return;
        }
        super.onBackPressed();
    }
}
