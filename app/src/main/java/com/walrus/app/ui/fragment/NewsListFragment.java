package com.walrus.app.ui.fragment;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.walrus.app.R;
import com.walrus.app.adapter.NewsAdapter;
import com.walrus.app.dp.News;
import com.walrus.app.model.NewsResponse;
import com.walrus.app.model.Resource;
import com.walrus.app.viewmodel.NewsListViewModel;

import java.util.ArrayList;

public class NewsListFragment extends Fragment implements NewsAdapter.ClickListener {

    private NewsAdapter mNewsAdapter;
    private ArrayList<News> mNews;
    private ClickListenerFromNewsList clickListenerFromNewsList;

    public static NewsListFragment newInstance() {
        return new NewsListFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        clickListenerFromNewsList = (ClickListenerFromNewsList) context;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.news_list_fragment, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        mNews = new ArrayList<>();
        mNewsAdapter = new NewsAdapter(getContext(), mNews, this);
        RecyclerView recyclerView = view.findViewById(R.id.rv_news);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(mNewsAdapter);
        view.findViewById(R.id.iv_close_button).setOnClickListener(v -> getActivity().finish());

        NewsListViewModel mViewModel = new ViewModelProvider(this).get(NewsListViewModel.class);
        mViewModel.getNews().observe(getViewLifecycleOwner(), new Observer<Resource<NewsResponse>>() {
            @Override
            public void onChanged(Resource<NewsResponse> newsResponseResource) {
                switch (newsResponseResource.status)
                {
                    case SUCCESS:
                        mNews.clear();
                        if (newsResponseResource.data != null) {
                            mNews.addAll(newsResponseResource.data.getArticles());
                            mNewsAdapter.notifyDataSetChanged();
                        }
                        break;

                    case ERROR:
                        if (newsResponseResource.data != null) {
                            Toast.makeText(getContext(), newsResponseResource.data.toString(),Toast.LENGTH_SHORT).show();
                        }
                        break;
                }
            }
        });
    }

    @Override
    public void onClickFullNews(int position) {
        clickListenerFromNewsList.onClickFullNews(mNews.get(position).getTitle(), mNews.get(position).getUrl());
    }

    public interface ClickListenerFromNewsList{
        void onClickFullNews(String title, String url);
    }
}