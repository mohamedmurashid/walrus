package com.walrus.app.util;

import com.walrus.app.dp.AppDatabase;
import com.walrus.app.network.NetworkClient;
import com.walrus.app.network.NetworkService;

public class Application extends android.app.Application {

    private static Application mInstance;
    private NetworkService mService;
    private AppDatabase mAppDatabase;

    public static synchronized Application getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        mService = NetworkClient.getClient().create(NetworkService.class);
        mAppDatabase = AppDatabase.getDatabase(this);
    }

    public NetworkService getNetworkService() {
        return mService;
    }

    public AppDatabase getAppDatabase() {
        return mAppDatabase;
    }


}
