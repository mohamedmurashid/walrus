package com.walrus.app.util;

public interface Constant {

    String DATABASE_NAME = "news_database";

    String NEWS_TABLE_NAME = "news_table";

    int NUMBER_OF_THREADS_DB_OPERATION = 4;

    String COUNTRY_QUERY = "us";

    String TAG = "Murashid";
}
