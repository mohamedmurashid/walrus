package com.walrus.app.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.walrus.app.model.NewsResponse;
import com.walrus.app.model.Resource;
import com.walrus.app.repository.NewsRepository;

public class NewsListViewModel extends ViewModel {
    private MutableLiveData<Resource<NewsResponse>> newsResponse;

    public NewsListViewModel() {
        NewsRepository mNewsRepository = new NewsRepository();
        newsResponse =  mNewsRepository.getNews();
    }

    public MutableLiveData<Resource<NewsResponse>> getNews() {
        return newsResponse;
    }

}